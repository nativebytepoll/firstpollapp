var express = require('express');
var usersModel=require('../modules/users');
var path=require('path');
var router = express.Router();

 //for image upload
/*router.use(express.static('public'));
router.get("/",(req,res)=>{
  res.sendFile(__dirname +'/login.ejs');
});
*/

/* GET home page. */
//middleware for username
function checkUsername(req,res,next){
  var uname=req.body.uname;
  var checkexituname=usersModel.findOne({username:uname});

  checkexituname.exec((err,data)=>{
    if(err) throw err;
    if(data){
    return res.render('signup', { title: 'Signup ',  msg:'Username all ready exit'});
    }
    next();
  });

}
//middleware for emailid
function checkEmail(req,res,next){
  var email=req.body.eid;
  var checkexitemail=usersModel.findOne({emailid:email});//to data find

  checkexitemail.exec((err,data)=>{
    if(err) throw err;
    if(data){
    return res.render('signup', { title: 'Signup ',  msg:'Email all ready exit'});
    }
    next();
  });

}

// for admin
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Admin '});
});

// for home
router.get('/home', function(req, res, next) {
  res.render('home', { title: 'Home Page ' });

});
// for home
router.post('/home', function(req, res, next) {
 res.render('home', { title: 'Home Page ' });
//  console.log("Rameshwar kumar");
});

// for login
router.get('/login', function(req, res, next) {
  res.render('login', { title: 'Login ',msg:'' });
});

// for login
router.post('/login', function(req, res, next) {
  var username=req.body.uname;
  var password=req.body.psw;
  var checkUsername=usersModel.findOne({username:username});//to data find

 checkUsername.exec((err,data)=>{
   if(err) throw err;

   var getPassword=data.password;

   if(password!= getPassword)
   {
     res.render('home', { title: 'Login ',msg:'Invalid username an password' });
   }
   else {
     res.render('home', { title: 'Login ',msg:'User logged in Successfully ' });
   }

 });


});

// for signup
router.get('/signup', function(req, res, next) {
  res.render('signup', { title: 'Signup ',msg:''});
});

// for signup
router.post('/signup',checkUsername,checkEmail,function(req, res, next) {
  //to get value from signup page
  var username=req.body.uname;
  var emailId=req.body.eid;
  var password=req.body.psw;
  var confPassword=req.body.confpsw;
if(password !=confPassword){
  res.render('signup', { title: 'Signup ',  msg:'Pasword not matched'});
}
else {


// insert data in pollo name db
  var userDetails=new usersModel({
      username: username,
      emailid:emailId,
      password:password
  });

  userDetails.save((err,doc)=>{
    if(err) throw err;
    res.render('signup', { title: 'Signup ',  msg:'Users Registered Successfully'});
  });
}
});

// for admin
router.get('/admin', function(req, res, next) {
  res.render('login', { title: 'Login ', msg:''});

});

module.exports = router;
